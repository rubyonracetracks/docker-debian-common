# Docker Debian - FAQ

## How do I build a Docker image on my local machine?
* You MUST have Docker installed.  How to do this is covered in the [Different Docker Tutorial](https://www.differentdockertutorial.com/).
* From the root directory of the Docker image build repository, enter the command "bash build.sh" to start the build process.  The logging output file is in the log directory.
* NOTE: This manual process is used for experimental purposes and is NOT used to push Docker images.

## How do you build and push Docker images automatically?
* The process of building and pushing the Docker image is handled by GitLab CI.  The .gitlab-ci.yml file contains the configurations.
* Note that the process of building the Docker image is a test.  The build process is set up to abort immediately in the event of an error.  This prevents a good Docker image in the repository from being replaced with a defective one.
* The new image created in the build process is uploaded to the GitLab registry ONLY when the build script is executed in the master branch.
* GitLab's cron scheduling feature is used to automatically build Docker images on a regular basis.

## Why do you prefer GitLab's Docker repository over Docker Cloud?
Docker Cloud offers only one private Docker image.  GitLab does not have this limitation.

## Why do you prefer using GitLab CI over Docker Cloud's automated build system?
* GitLab CI is faster.  Docker Cloud's automated builds are often queued.
* GitLab CI offers cron scheduling.  This makes it possible to ensure a steady supply of new Docker images so that you don't get stuck with old images.

## Why do you prefer RVM over other Ruby version manager software?
* I was unable to make patch upgrades to the Ruby version in my Rails apps under rbenv.  When I tried to upgrade Rails apps from Ruby 2.6.0 to 2.6.3 (by updating the .ruby-version file and Gemfile), I was getting the error message "Your Ruby version is 2.6.0, but your Gemfile specified 2.6.3."  Although I had both versions of Ruby installed in the Docker image, entered the commands "rbenv local 2.6.3" and "rbenv global 2.6.3", and followed other suggestions I had seen online, I still couldn't resolve this issue.  So I tried a similar setup with RVM instead of rbenv, and I was able to upgrade the app without any problems.
* I tried asdf.  All "rails" commands had to begin with "bundle exec", and I didn't feel like changing the Bash scripts in all of my Rails apps to reflect this.
* I tried chruby.  It's more difficult to automate the process of installing chruby than it is to automate the process of installing RVM.  Given that its underlying mechanisms are much simpler than those of rbenv and RVM (which means fewer things that can break), I understand why chruby would appeal to those who rely on their host systems as their development environments.  If RVM didn't exist, I'd probably use chruby.