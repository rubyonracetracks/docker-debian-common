# Docker Image Build Schedules

This page contains links to the cron schedules of Docker builds.  NOTE: I have no idea how to make these pipeline schedules publicly accessible.  These links are for my personal reference.

## rubyonracetracks/docker-debian-buster-*
* [min-stage1](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-stage1/pipeline_schedules)
* [min-stage2](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-stage2/pipeline_schedules)
* [min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rvm/pipeline_schedules)
* [rvm-rails-general](https://gitlab.com/rubyonracetracks/docker-debian-buster-rvm-rails-general/pipeline_schedules)
* [min-rbenv](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv/pipeline_schedules)
* [rbenv-rails-general](https://gitlab.com/rubyonracetracks/docker-debian-buster-rbenv-rails-general/pipeline_schedules)

## rubyonracetracks/docker-debian-stretch-*
* [min-stage1](https://gitlab.com/rubyonracetracks/docker-debian-stretch-min-stage1/pipeline_schedules)
* [min-stage2](https://gitlab.com/rubyonracetracks/docker-debian-stretch-min-stage2/pipeline_schedules/)
* [min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-stretch-min-rvm/pipeline_schedules/)

## jhsu802701/docker-debian-buster-*
* [rvm-rails-rubymn2](https://gitlab.com/jhsu802701/docker-debian-buster-rvm-rails-rubymn2/pipeline_schedules)
* [rbenv-rails-rubymn2](https://gitlab.com/jhsu802701/docker-debian-buster-rbenv-rails-rubymn2/pipeline_schedules)
* [rbenv-rails-octobox](https://gitlab.com/jhsu802701/docker-debian-buster-rbenv-rails-octobox/pipeline_schedules)

## jhsu802701/docker-debian-stretch-*
* [rvm-rails-sessionizer](https://gitlab.com/jhsu802701/docker-debian-stretch-rvm-rails-sessionizer/pipeline_schedules)
* [rbenv-rails-sessionizer](https://gitlab.com/jhsu802701/docker-debian-stretch-rbenv-rails-sessionizer/pipeline_schedules)
* [rbenv-rails-codetriage](https://gitlab.com/jhsu802701/docker-debian-stretch-rbenv-rails-codetriage/pipeline_schedules)
