#!/bin/bash

# This script restarts Docker.

echo '---------------------------'
echo 'sudo service docker restart'
sudo service docker restart

echo '--------------------------'
echo 'sudo service docker status'
sudo service docker status
