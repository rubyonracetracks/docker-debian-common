#!/bin/bash
set -e

source variables.sh

echo '************************************'
echo "Docker image to build: $DOCKER_IMAGE"
echo '************************************'

# Skip deleting containers and images in the GitLab CI environment
# to avert a non-zero exit.
if [[ -z "$GITLAB_CI" && -z "$TRAVIS" ]]
then
  wget -O - https://gitlab.com/rubyonracetracks/docker-debian-common/raw/master/delete-containers.sh | bash -s "$DOCKER_CONTAINER"
  wget -O - https://gitlab.com/rubyonracetracks/docker-debian-common/raw/master/delete-images.sh | bash -s "$DOCKER_IMAGE"
fi

DATE=`date +%Y_%m%d_%H%M_%S`
echo '--------------------------------'
echo 'Date and time at start of build:'
echo $DATE
DIR_LOG=$PWD/log
mkdir -p $DIR_LOG

echo '****************************'
echo "BEGIN building $DOCKER_IMAGE"
echo '****************************'

docker build -t $DOCKER_IMAGE . 2>&1 | tee $DIR_LOG/build-$DATE.txt

echo '*******************************'
echo "Finished building $DOCKER_IMAGE"
echo '*******************************'

echo '------------------------------'
echo 'Date and time at end of build:'
date +%Y_%m%d_%H%M_%S

echo '-------------------------------------'
echo "docker images -a | grep $DOCKER_IMAGE"
docker images -a | grep $DOCKER_IMAGE