#!/bin/bash
set -e

# This script is used in continuous integration for testing a new Docker 
# image before pushing it.

# There should be only one Docker container available.

DOCKER_IMAGE=$1

echo '-----------------------------------------'
echo "BEGIN: testing Docker image $DOCKER_IMAGE"
echo '-----------------------------------------'

for i in $(docker images -a | grep $DOCKER_IMAGE | awk '{print $3}')
do
  SCRIPT_TO_RUN='/usr/local/bin/check' # Script to run
  docker create -i -t -u='winner' $DOCKER_IMAGE $SCRIPT_TO_RUN
  wait
done;

echo '--------------------------------------------'
echo "FINISHED: testing Docker image $DOCKER_IMAGE"
echo '--------------------------------------------'

